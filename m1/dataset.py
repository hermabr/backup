from pathlib import Path
from typing import Tuple

import numpy as np
from PIL import Image
from torch import tensor
from torch.utils.data import Dataset
from torchvision import transforms
from tqdm import tqdm

from m1.config import CIFAR_DATA_PATH, IMAGENET_DATA_PATH


class MandatoryDataset(Dataset):
    def __init__(self, files: Tuple[str, Path]):
        self.X = []
        self.y = []
        self.filenames = []

        self.preprocess = transforms.Compose(
            [
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
                transforms.Normalize(
                    mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
                ),
            ]
        )

        for label, file in tqdm(files, leave=False, desc="Load image dataset"):
            self.X.append(self.preprocess(Image.open(file)))
            self.y.append(label)
            self.filenames.append(str(file))

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], self.y[idx], self.filenames[idx]


class CifarDataset(Dataset):
    def __init__(self):
        import pickle

        with open(CIFAR_DATA_PATH / "data_batch_1", "rb") as f:
            data = pickle.load(f, encoding="bytes")

        self.X = []
        for img in np.reshape(data[b"data"][:200], (200, 3, 32, 32)):
            self.X.append(
                transforms.Compose(
                    [transforms.ToTensor(), transforms.Resize((224, 224))]
                )(img.transpose())
            )

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], 0, 0


class ImagenetDataset(Dataset):
    def __init__(self):
        self.X = []
        for file in list(IMAGENET_DATA_PATH.glob("*"))[:200]:
            img = np.array(Image.open(file))
            self.X.append(
                transforms.Compose(
                    [transforms.ToTensor(), transforms.Resize((224, 224))]
                )(img)
            )

            if self.X[-1].size(0) == 1:
                self.X[-1] = self.X[-1].repeat(3, 1, 1)

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], 0, 0
