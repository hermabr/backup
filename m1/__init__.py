import matplotlib.pyplot as plt
from typing import Union
from dataclasses import dataclass
import torch
from pathlib import Path
from PIL import Image
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import classification_report, confusion_matrix
from torch.hub import load as load_torch
from torch.utils.data import DataLoader
from torchvision.models.resnet import ResNet18_Weights
from tqdm import tqdm
import numpy as np

from m1.config import (
    BATCH_SIZE,
    OUTPUT_PATH,
    DEVICE,
    EPOCHS,
    LABELS,
    LEARNING_RATES,
    MODEL_PATH,
    TRAIN_FILES,
    VAL_FILES,
    TEST_FILES,
    FEATURE_MAPS_TO_INCLUDE,
    set_seed,
)
from m1.dataset import MandatoryDataset, ImagenetDataset, CifarDataset


criterion = nn.CrossEntropyLoss()


print(f"Device: {DEVICE}")


def calculate_accuracy(y, y_hat):
    conf_matrix = confusion_matrix(y, y_hat)
    class_wise_accuracy = np.diag(conf_matrix) / conf_matrix.sum(axis=1)
    return class_wise_accuracy, np.mean(class_wise_accuracy)


def train(model, dataloader, criterion, optimizer):
    model.train()

    losses = []

    all_ys = []
    all_y_hats = []

    for X, y, _ in tqdm(dataloader, leave=False, desc="train"):
        X = X.to(DEVICE)
        y = y.to(DEVICE)

        optimizer.zero_grad()

        y_hat = model(X)

        loss = criterion(y_hat, y)
        loss.backward()
        optimizer.step()

        losses.append(loss)

        all_ys += y.cpu()
        all_y_hats += torch.argmax(y_hat, dim=1).cpu()

    per_class_acc = calculate_accuracy(all_ys, all_y_hats)

    report = classification_report(
        torch.tensor(all_ys).detach(),
        torch.tensor(all_y_hats).detach(),
        output_dict=False,
        target_names=LABELS,
    )

    print(per_class_acc)
    print(report)

    return (sum(losses) / len(losses)).item()


def val(model, dataloader, criterion):
    model.eval()

    losses = []

    all_ys = []
    all_y_hats = []

    with torch.no_grad():
        for X, y, _ in tqdm(dataloader, leave=False, desc="val"):
            X = X.to(DEVICE)
            y = y.to(DEVICE)

            y_hat = model(X)

            loss = criterion(y_hat, y)

            losses.append(loss)

            all_ys += y.cpu()
            all_y_hats += torch.argmax(y_hat, dim=1).cpu()

    per_class_acc = calculate_accuracy(all_ys, all_y_hats)

    report = classification_report(
        torch.tensor(all_ys).detach(),
        torch.tensor(all_y_hats).detach(),
        output_dict=False,
        target_names=LABELS,
    )

    print(per_class_acc)
    print(report)

    return (sum(losses) / len(losses)).item()


def test(model, dataloader, criterion, should_save_images=False):
    model.eval()

    all_ys = []
    all_y_hats = []
    all_softmaxes = []
    all_filenames = []

    losses = []
    with torch.no_grad():
        for X, y, filename in tqdm(dataloader, leave=False, desc="test"):
            X = X.to(DEVICE)
            y = y.to(DEVICE)

            y_hat = model(X)

            loss = criterion(y_hat, y)
            losses.append(loss)

            all_ys += y.cpu()
            all_y_hats += torch.argmax(y_hat, dim=1).cpu()
            all_softmaxes += torch.softmax(y_hat, dim=1).cpu()
            all_filenames += filename

    all_ys = torch.tensor(all_ys)
    all_y_hats = torch.tensor(all_y_hats)
    all_softmaxes = torch.stack(all_softmaxes)

    if should_save_images:

        def save_images(filepaths, filename):
            best_img = Image.new("RGB", (750, 300))
            for i, filepath in enumerate(filepaths):
                img = Image.open(filepath)
                best_img.paste(img, ((i % 5) * 150, (i // 5) * 150))
            best_img.save(filename, quality=95)

        for label_idx, label in enumerate(LABELS[:3]):
            _, best_indices = torch.topk(all_softmaxes[:, label_idx], 10)
            save_images(
                [all_filenames[x] for x in best_indices],
                OUTPUT_PATH / f"best_{label}.jpg",
            )

            _, worst_indices = torch.topk(-all_softmaxes[:, label_idx], 10)
            save_images(
                [all_filenames[x] for x in worst_indices],
                OUTPUT_PATH / f"worst_{label}.jpg",
            )

    accuracy = calculate_accuracy(all_ys, all_y_hats)

    report = classification_report(
        all_ys.clone().detach(),
        all_y_hats.clone().detach(),
        output_dict=False,
        target_names=LABELS,
    )

    print("Accuracy")
    for i, label in enumerate(LABELS):
        print(f"{label:<{max(len(x) for x in LABELS)}}: {accuracy[0][i]}")
    print(f"{'Avg':<{max(len(x) for x in LABELS)}}: {accuracy[1]}")
    print()
    print(report)

    return (sum(losses) / len(losses)).item()


@dataclass
class WeightsDefault:
    pass


@dataclass
class WeightsFromPath:
    load_path: Path


Weights = Union[WeightsDefault, WeightsFromPath]


def load_model(weights: Weights):
    model = load_torch(
        "pytorch/vision:v0.10.0",
        "resnet18",
        weights=ResNet18_Weights.DEFAULT
        if isinstance(weights, WeightsDefault)
        else None,
    )
    model.fc = nn.Linear(512, len(LABELS))
    model = model.to(DEVICE)
    if isinstance(weights, WeightsFromPath):
        model.load_state_dict(torch.load(weights.load_path))

    return model


def fit():
    set_seed()
    train_dataset = MandatoryDataset(TRAIN_FILES)
    val_dataset = MandatoryDataset(VAL_FILES)
    train_dataloader = DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    val_dataloader = DataLoader(val_dataset, batch_size=BATCH_SIZE, shuffle=True)

    best_loss = torch.inf
    best_lr = -1
    best_epoch = -1

    for learning_rate in LEARNING_RATES:
        print(f"Learning rate: {learning_rate}")

        model = load_model(WeightsDefault())

        optimizer = optim.Adam(model.parameters(), lr=learning_rate)
        for i in range(EPOCHS):
            print(f"Epoch {i+1}")
            train_loss = train(model, train_dataloader, criterion, optimizer)
            print("Train:", train_loss)
            val_loss = val(model, val_dataloader, criterion)
            print("Val:", val_loss)
            if val_loss < best_loss:
                print("\x1b[33mnew best loss:", val_loss, "\x1b[0m")
                best_loss = val_loss
                best_lr = learning_rate
                best_epoch = i
                torch.save(model.state_dict(), MODEL_PATH)
        print()

    print("BEST:")
    print(best_loss)
    print(best_lr)
    print(best_epoch)


def run_epoch(model, dataloader):
    model.eval()

    with torch.no_grad():
        for X, _, _ in tqdm(dataloader, leave=False, desc="epoch"):
            X = X.to(DEVICE)

            y_hat = model(X)


def hook_non_positive(module, input, output):
    non_pos_percentage = ((output <= 0).sum() / output.numel()).item()
    non_pos_percentages[module.name] = (
        non_pos_percentages[module.name] * non_pos_num[module.name]
        + non_pos_percentage * output.size(0)
    ) / (non_pos_num[module.name] + output.size(0))
    non_pos_num[module.name] += output.size(0)


def hook_mean(module, input, output):
    means = output.mean(dim=[2, 3])

    a = current_feature_map_batch_idx[0]
    b = current_feature_map_idx[0]
    all_feature_maps[
        a : a + means.size(0),
        b : b + means.size(1),
    ]

    current_feature_map_batch_idx[0] += means.size(0)
    current_feature_map_idx[0] += means.size(1)
    if module.idx == 5:
        current_feature_map_idx[0] -= current_feature_map_idx[0]


def reproduction():
    set_seed()
    global DEVICE
    DEVICE = torch.device("cpu")
    test_dataset = MandatoryDataset(TEST_FILES)
    test_dataloader = DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True)

    model = load_model(WeightsFromPath(MODEL_PATH))

    test_loss = test(model, test_dataloader, criterion, should_save_images=True)
    print("Test loss:", test_loss)


def task2():
    set_seed()
    model = load_model(weights=WeightsFromPath(MODEL_PATH))

    global non_pos_num, non_pos_percentages
    non_pos_num = {}
    non_pos_percentages = {}

    modules_included = []
    named_modules = list(model.named_modules())
    for name, module in named_modules[
        (len(named_modules) - 1)
        % (len(named_modules) // FEATURE_MAPS_TO_INCLUDE) :: len(named_modules)
        // FEATURE_MAPS_TO_INCLUDE
    ]:
        module.register_forward_hook(hook_non_positive)
        module.name = name

        modules_included.append(name)
        non_pos_percentages[name] = 0
        non_pos_num[name] = 0

    test_dataset = MandatoryDataset(list(TEST_FILES)[:200])
    test_dataloader = DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True)

    run_epoch(model, test_dataloader)

    print("Modules and values")
    for name in modules_included:
        print(
            f"{name:<{max(len(x) for x in modules_included)}}: {non_pos_percentages[name]}"
        )


def task3():
    set_seed()
    for task3_setup in ["normal", "no_finetuning", "imagenet", "cifar_10"]:
        print(f"Running task3 with {task3_setup}")
        model = load_model(
            weights=WeightsDefault()
            if task3_setup == "no_finetuning"
            else WeightsFromPath(MODEL_PATH)
        )

        modules_included = []
        named_modules = list(model.named_modules())
        for idx, (name, module) in enumerate(
            named_modules[:: len(named_modules) // FEATURE_MAPS_TO_INCLUDE]
        ):
            if name == "":
                continue

            module.register_forward_hook(hook_mean)
            module.name = name
            module.idx = idx
            modules_included.append(name)

        if len(modules_included) != 5:
            raise RuntimeError("Not 5 modules in the modules list")

        global all_feature_maps, current_feature_map_batch_idx, current_feature_map_idx
        all_feature_maps = torch.empty((200, sum([64, 128, 256, 512, 512])))
        current_feature_map_batch_idx = [0]
        current_feature_map_idx = [0]

        if task3_setup == "imagenet":
            test_dataset = ImagenetDataset()
        elif task3_setup == "cifar_10":
            test_dataset = CifarDataset()
        else:
            test_dataset = MandatoryDataset(list(TEST_FILES)[:200])
        test_dataloader = DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True)

        run_epoch(model, test_dataloader)

        means = all_feature_maps.unsqueeze(dim=2)
        print(means.shape)

        term_1 = (means @ means.transpose(1, 2)).mean(dim=0)

        e_f = means.mean(dim=0)
        term_2 = e_f @ e_f.T

        res = term_1 - term_2
        print(res)
        print(res.min(), res.max())
        print()

        eigvals = torch.linalg.eig(res).eigenvalues.abs()

        _, vals = eigvals.topk(1000)
        print(vals.shape)
        sorted_indices = vals.argsort(descending=True)
        vals = vals[sorted_indices]

        plt.bar([x for x in range(1, 1001)], vals)

        plt.savefig(OUTPUT_PATH / f"{task3_setup}.png")
        plt.close()


def run_all():
    fit()
    reproduction()
    task2()
    task3()
