import random
from torch.utils.data import random_split
from pathlib import Path

import numpy as np
import torch

DATA_PATH = (
    Path("mandatory1_data")
    if Path("mandatory1_data").exists()
    else Path("/itf-fi-ml/shared/courses/IN3310/mandatory1_data")
)

CIFAR_DATA_PATH = (
    Path("cifar-10-batches-py")
    if Path("imagenet_val").exists()
    else Path("/itf-fi-ml/shared/courses/IN3310/cifar-10-batches-py")
)

IMAGENET_DATA_PATH = (
    Path("imagenet_val")
    if Path("imagenet_val").exists()
    else Path("/itf-fi-ml/shared/courses/IN3310/imagenet_val")
)

OUTPUT_PATH = Path("output")
OUTPUT_PATH.mkdir(parents=True, exist_ok=True)
MODEL_PATH = OUTPUT_PATH / Path("best_model.pt")

BATCH_SIZE = 16

TEST_SIZE = int(3e3)
VAL_SIZE = int(2e3)

EPOCHS = 10
LEARNING_RATES = [0.0001, 0.00001, 0.000001]
FEATURE_MAPS_TO_INCLUDE = 5

LABELS = ["buildings", "forest", "glacier", "mountain", "sea", "street"]


def set_seed(seed: int = 42):
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


_all_files = []

for folder in DATA_PATH.glob("*"):
    label = LABELS.index(folder.name)
    files = folder.glob("*")
    for file in folder.glob("*"):
        _all_files.append((label, file))

TRAIN_FILES, VAL_FILES, TEST_FILES = random_split(
    _all_files, [len(_all_files) - TEST_SIZE - VAL_SIZE, VAL_SIZE, TEST_SIZE]
)


def validate_disjointness(all_items, subset_1, subset_2, subset_3):
    assert len(all_items) == len(subset_1) + len(subset_2) + len(subset_3)
    assert len(set(all_items)) == len(set(subset_1) | set(subset_2) | set(subset_3))
    assert len(subset_2) == int(2e3)
    assert len(subset_3) == int(3e3)


validate_disjointness(_all_files, TRAIN_FILES, VAL_FILES, TEST_FILES)
