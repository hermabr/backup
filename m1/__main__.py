from m1 import run_all, fit, reproduction, task2, task3

import sys


def help_msg():
    print(f"Usage: python -m m1 [all/fit/reproduction/task2/task3]")
    exit(1)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        help_msg()

    if sys.argv[1] == "all":
        run_all()
    elif sys.argv[1] == "fit":
        fit()
    elif sys.argv[1] == "reproduction":
        reproduction()
    elif sys.argv[1] == "task2":
        task2()
    elif sys.argv[1] == "task3":
        task3()
    else:
        help_msg()
