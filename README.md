# Mandatory exercise 1 IN4310

Python files are in the `m1` folder

## Installation

```bash
pip3.X install torch torchvision scikit-learn tqdm matplotlib
```

## Usage

Add datasets to
`./mandatory1_data` or `/itf-fi-ml/shared/courses/IN3310/mandatory1_data`
`./cifar-10-batches-py` or `/itf-fi-ml/shared/courses/IN3310/cifar-10-batches-py`
`./imagenet_val` or `/itf-fi-ml/shared/courses/IN3310/imagenet_val`

an alternative is to override `DATA_PATH`, `CIFAR_DATA_PATH` and `IMAGENET_DATA_PATH` in `m1/config.py`

### Reproduction routine

```bash
python3.X reproduction.py
```

### Running other routines

```bash
python3.X -m m1 [all/fit/reproduction/task2/task3]
```
